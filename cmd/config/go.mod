module gitlab.com/metakeule/config/cmd/config

go 1.16

replace gitlab.com/metakeule/config => ../../

require gitlab.com/metakeule/config v1.21.2
