package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	// "flag"
	// "fmt"
	// "os"
	"os/exec"

	"gitlab.com/metakeule/config"
)

const defaultplattforms = "darwin/amd64 linux/amd64 linux/arm linux/arm64 windows/amd64"

var (
	cfg           = config.MustNew("config", config.VERSION, "a polyglot tool to set the configuration of programs")
	optionProgram = cfg.LastString("program", "the program in question (must be compatible with config and must reside in the PATH)", config.Required)
	//optionProgram           = cfg.NewString("program", "the program where the options belong to (must be a config compatible program)", config.Required, config.Shortflag('p'))
	optionLocations = cfg.NewBool("locations", "show the locations where the options are currently set / overriden", config.Shortflag('l'))

	cfgSet            = cfg.MustCommand("set", "set the value of an option in the configuration file").Skip("locations")
	optionSetKey      = cfgSet.NewString("option", "the option in question", config.Required, config.Shortflag('o'))
	optionSetValue    = cfgSet.NewString("value", "the value", config.Required, config.Shortflag('v'))
	optionSetPathType = cfgSet.NewString("type", "the kind of the config file where the option is stored (global,user or local).", config.Shortflag('t'), config.Required)
	optionSetSubCmd   = cfgSet.NewString("cmd", "the command, if the option belongs to a (sub)command of the program", config.Shortflag('c'))

	cfgGet          = cfg.MustCommand("get", "get the resulting value based on the configuration files and environmental variables").Skip("locations")
	optionGetKey    = cfgGet.NewString("option", "the option in question, if not set, all prefilled options are returned", config.Shortflag('o'))
	optionGetSubCmd = cfgGet.NewString("cmd", "the command, if the option belongs to a (sub)command of the program", config.Shortflag('c'))

	cfgPath        = cfg.MustCommand("path", "show the locations of the configuration files").Skip("locations")
	optionPathType = cfgPath.NewString("type", "the kind of the config file (global,user,local or all).", config.Shortflag('t'), config.Default("all"))

	cfgRelease              = cfg.MustCommand("release", "release a new version of a Go program").Skip("program").Skip("locations")
	optionReleaseVersionDir = cfgRelease.NewString("versiondir", "directory where the version files reside", config.Default("."))
	optionReleasePackage    = cfgRelease.NewString("package", "name of the package for the version.go file", config.Default("main"))
	optionReleaseType       = cfgRelease.NewString("type", "the kind of the release ("+config.MAJOR+","+config.MINOR+","+config.PATCH+")", config.Shortflag('t'), config.Default(config.PATCH))
	optionReleaseGittag     = cfgRelease.NewBool("gittag", "add the version as a gittag", config.Default(true), config.Shortflag('g'))

	cfgBuild              = cfg.MustCommand("build", "build executeables from a Go package for multiple plattforms").Skip("locations").Relax("program")
	optionBuildDir        = cfgBuild.NewString("dir", "the directory where the builds are stored", config.Shortflag('d'), config.Default("artifacts"))
	optionBuildPlattforms = cfgBuild.NewString("plattforms", "the plattforms for the builds, separated by space. an empty string results in builds on every plattform (normal go plattforms + pseudo-plattform "+config.PseudoPlattformWinCGO+")", config.Default(defaultplattforms))
	optionBuildVersionate = cfgBuild.NewBool("versionate", "write the current version into the filename", config.Default(false), config.Shortflag('v'))
	optionBuildVersionDir = cfgBuild.NewString("versiondir", "directory where the version files reside", config.Default("."))
)

func GetVersion(cmdpath string) (string, error) {
	cmd := exec.Command(cmdpath, "--version")
	out, err := cmd.Output()
	if err != nil {
		//return "", err
		return "", fmt.Errorf("%s", out)
	}
	// fmt.Printf("version: %#v\n", string(out))
	v := strings.Split(strings.TrimSpace(string(out)), " ")
	if len(v) != 3 {
		return "", fmt.Errorf("%s --version returns invalid result: %#v", cmdpath, string(out))
	}
	return strings.TrimSpace(v[2]), nil
}

func GetSpec(cmdpath string, c *config.Config) error {
	cmd := exec.Command(cmdpath, "--config-spec")
	out, err := cmd.Output()
	if err != nil {
		return fmt.Errorf("%s does not seem to be compatible with config", cmdpath)
		// return err
	}
	return c.UnmarshalJSON(out)
}

func writeErr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		fmt.Fprintln(os.Stdout, " -> run 'config help' to get more help")
		os.Exit(1)
	}
}

func cmdPrintAll() error {
	if optionLocations.IsSet() {
		if cmdConfig == nil {
			return fmt.Errorf("missing command")
		}
		err := cmdConfig.Load(false)
		if err != nil {
			return fmt.Errorf("Can't load options for command %s: %s", cmd, err.Error())
			// os.Exit(1)
		}
		locations := map[string][]string{}

		cmdConfig.EachValue(func(name string, value interface{}) {
			locations[name] = cmdConfig.Locations(name)
		})

		cmdConfig.EachCommand(func(cmdname string, ccConf *config.Config) {
			ccConf.EachValue(func(name string, value interface{}) {
				locations[cmdname+"."+name] = ccConf.Locations(name)
			})
		})

		var b []byte
		b, err = json.Marshal(locations)
		if err != nil {
			return fmt.Errorf("Can't print locations for command %s: %s", cmd, err.Error())
			// os.Exit(1)
		}

		fmt.Fprintln(os.Stdout, string(b))
		// os.Exit(0)
	} else {
		if cmdConfig == nil {
			return fmt.Errorf("missing command")
		}
		err := cmdConfig.Load(false)
		if err != nil {
			return fmt.Errorf("Can't load options for command %s: %s", cmd, err.Error())
			// os.Exit(1)
		}
		values := map[string]interface{}{}

		cmdConfig.EachValue(func(name string, value interface{}) {
			values[name] = value
		})

		cmdConfig.EachCommand(func(cmdname string, ccConf *config.Config) {
			ccConf.EachValue(func(name string, value interface{}) {
				values[cmdname+"."+name] = value
			})
		})

		var b []byte
		b, err = json.Marshal(values)
		if err != nil {
			return fmt.Errorf("Can't print values for command %s: %s", cmd, err.Error())
			// os.Exit(1)
		}

		fmt.Fprintln(os.Stdout, string(b))
	}
	return nil
}

var cmdConfig *config.Config
var commandPath string
var cmd string

func main() {

	err := cfg.Run()
	writeErr(err)

	command := cfg.ActiveCommand()

	if command == cfgRelease {
		vdir := optionReleaseVersionDir.Get()
		if vdir == "." {
			vdir, _ = os.Getwd()
		}
		oldversion, newversion, err := config.Release(vdir, optionReleaseType.Get(), optionReleaseGittag.Get(), optionReleasePackage.Get())
		fmt.Printf("%q -> %q\n", oldversion, newversion)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Can't do release of type %q in dir %q:\n %s\n", optionReleaseType.Get(), vdir, err.Error())
			os.Exit(1)
		}
		os.Exit(0)
	}

	if command == cfgBuild {
		vdir := optionBuildVersionDir.Get()
		if vdir == "." {
			vdir, _ = os.Getwd()
		}
		wd, _ := os.Getwd()

		prog := optionProgram.Get()
		if prog == "" {
			prog = filepath.Base(wd)
		}
		errs := config.GoBuild(wd, optionBuildDir.Get(), prog, optionBuildPlattforms.Get(), optionBuildVersionate.Get(), vdir)
		for _, err := range errs {
			fmt.Fprintln(os.Stderr, err.Error())
		}

		if len(errs) == 0 {
			fmt.Fprintln(os.Stdout, "everything did build successfully")
			os.Exit(0)
		}

		os.Exit(1)
	}

	cmd = optionProgram.Get()
	commandPath, err = exec.LookPath(cmd)
	if err != nil {
		err = fmt.Errorf("can't find program %q in path: %v", cmd, err)
	}
	writeErr(err)
	var version string
	version, err = GetVersion(commandPath)
	if err != nil {
		err = fmt.Errorf("can't get version of program %q: %v", cmd, err)
	}
	writeErr(err)

	cmdConfig, err = config.New(filepath.Base(cmd), version, "")
	if err != nil {
		err = fmt.Errorf("can't create config for program %q: %v", cmd, err)
	}
	writeErr(err)
	err = GetSpec(commandPath, cmdConfig)
	if err != nil {
		err = fmt.Errorf("can't get spec for program %q: %v", cmd, err)
	}
	writeErr(err)

	if command == nil {
		err = cmdPrintAll()
		writeErr(err)
		return
	}

	switch command {
	// fmt.Println("no subcommand")
	case cfgGet:
		err := cmdConfig.Load(false)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Can't load config options for program %s: %s", cmd, err.Error())
			os.Exit(1)
		}

		if optionGetSubCmd.IsSet() {
			cmdConfig, err = cmdConfig.GetCommandConfig(optionGetSubCmd.Get())
			writeErr(err)
		}

		if !optionGetKey.IsSet() {
			var vals = map[string]interface{}{}
			cmdConfig.EachValue(func(name string, value interface{}) {
				vals[name] = value
			})
			var b []byte
			b, err = json.Marshal(vals)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Can't print locations for program %s: %s", cmd, err.Error())
				os.Exit(1)
			}

			fmt.Fprintln(os.Stdout, string(b))
			os.Exit(0)
		} else {
			key := optionGetKey.Get()
			if !cmdConfig.IsOption(key) {
				fmt.Fprintf(os.Stderr, "unknown option %s", key)
				os.Exit(1)
			}

			val := cmdConfig.GetValue(key)
			// cmdConfig.
			fmt.Fprintf(os.Stdout, "%v\n", val)
		}

	case cfgSet:
		key := optionSetKey.Get()
		val := optionSetValue.Get()
		ty := optionSetPathType.Get()
		setter := cmdConfig
		//fmt.Printf("cmdconfig: %#v\n", cmdConfig)
		if optionSetSubCmd.IsSet() {
			setter, err = cmdConfig.GetCommandConfig(optionSetSubCmd.Get())
			writeErr(err)
		}

		switch ty {
		case "user":
			if err := cmdConfig.LoadUser(); err != nil {
				fmt.Fprintf(os.Stderr, "Can't load user config file: %s", err.Error())
				os.Exit(1)
			}

			if err := setter.Set(key, val, cmdConfig.UserFile()); err != nil {
				fmt.Fprintf(os.Stderr, "Can't set option %#v to value %#v: %s", key, val, err.Error())
				os.Exit(1)
			}
			if err := cmdConfig.SaveToUser(); err != nil {
				fmt.Fprintf(os.Stderr, "Can't save user config file: %s", err.Error())
				os.Exit(1)
			}
		case "local":
			if err := cmdConfig.LoadLocals(); err != nil {
				fmt.Fprintf(os.Stderr, "Can't load local config file: %s", err.Error())
				os.Exit(1)
			}
			if err := setter.Set(key, val, cmdConfig.LocalFile()); err != nil {
				fmt.Fprintf(os.Stderr, "Can't set option %#v to value %#v: %s", key, val, err.Error())
				os.Exit(1)
			}
			if err := cmdConfig.SaveToLocal(); err != nil {
				fmt.Fprintf(os.Stderr, "Can't save local config file: %s", err.Error())
				os.Exit(1)
			}
		case "global":
			if err := cmdConfig.LoadGlobals(); err != nil {
				fmt.Fprintf(os.Stderr, "Can't load global config file: %s", err.Error())
				os.Exit(1)
			}
			if err := setter.Set(key, val, cmdConfig.FirstGlobalsFile()); err != nil {
				fmt.Fprintf(os.Stderr, "Can't set option %#v to value %#v: %s", key, val, err.Error())
				os.Exit(1)
			}
			if err := cmdConfig.SaveToGlobals(); err != nil {
				fmt.Fprintf(os.Stderr, "Can't save global config file: %s", err.Error())
				os.Exit(1)
			}
		default:
			fmt.Fprintf(os.Stderr, "'%s' is not a valid value for type option. possible values are 'local', 'global' or 'user'", ty)
			os.Exit(1)

		}
	case cfgPath:
		ty := optionPathType.Get()
		switch ty {
		case "user":
			fmt.Fprintln(os.Stdout, cmdConfig.UserFile())
			os.Exit(0)
		case "local":
			fmt.Fprintln(os.Stdout, cmdConfig.LocalFile())
			os.Exit(0)
		case "global":
			fmt.Fprintln(os.Stdout, cmdConfig.FirstGlobalsFile())
			os.Exit(0)
		case "all":
			paths := map[string]string{
				"user":   cmdConfig.UserFile(),
				"local":  cmdConfig.LocalFile(),
				"global": cmdConfig.FirstGlobalsFile(),
			}
			b, err := json.Marshal(paths)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Can't print locations for program %s: %s", cmd, err.Error())
				os.Exit(1)
			}

			fmt.Fprintln(os.Stdout, string(b))
			os.Exit(0)
		default:
			fmt.Fprintf(os.Stderr, "'%s' is not a valid value for type option. possible values are 'local', 'global' or 'user'", ty)
			os.Exit(1)
		}
	// some not allowed subcommand, should already be catched by config.Run
	default:
		panic("must not happen")

	}

}

/*
tool to read and set configurations

keys consist of names that are all uppercase letters separated by underscore _

config [binary] key

returns type: value

supported types are:
bool, int32, float32, string (utf-8), datetime, json

(this reads config)

config [binary] -l key1=value1,key2=value2 // sets the options in the local config file (relative to dir)
config [binary] -u key1=value1,key2=value2 // sets the options in the user config file
config [binary] -g key1=value1,key2=value2 // sets the options in the global config file
config [binary] -c key1=value1,key2=value2 // checks the options for the binary
config [binary] -h key                     // prints help about the key
config [binary] -h                         // prints help about all options
config [binary] -m key1=value1,key2=value2 // merges the options with global/user/local ones and prints the result

each setting of an option is checked for validity of the type.
for json values it is only checked, if it is valid json. additional
checks for the json structure must be done by the binary

values are passed the following way:
boolean values: true|false
int32 values: 34523
float32 values: 4.567
string values: "here the utf-8 string"
datetime values: 2006-01-02T15:04:05Z07:00    (RFC3339)
json values: '{"a": "\'b\'"}'

a binary that is supported by config is supposed to be callable with --config-spec and then return a json encoded hash of the options in the form of
[
	{
		"key": "here_the_key1",
	  "required": true|false,
	  "type": "bool"|"int32"|"float32"|"string"|"datetime"|"json",
	  "description": "...",
	  "default": null|"value"
	},
  {
  	"key": "here_the_key2",
	  required: true|false,
	  type: "bool"|"int32"|"float32"|"string"|"datetime"|"json",
	  description: "...",
	  "default": null|"value"
	}
	[...]
]

config is meant to be used on the plattforms:
- linux
- windows
- mac os x
(maybe Android too)

it builds the configuration options by starting with defaults and merging in the following configurations
(the next overwriting the same previously defined key):

defaults as reported via [binary] --config-spec
plattform-specific global config
plattform-specific user config
local config in directory named .config/[binary] in the current working directory
environmental variables starting with [BINARY]_CONFIG_
given args to the commandline

the binary itself wants to get all options in a single go.
it therefore may run

  config [binary] -args argstring

additionally there is a library for go (and might be created for other languages)
that make it easy to query the final options in a type-safe manner

subcommands are handled as if they were extra binaries with the name
[binary]_[subcommand]: they have separate config files and if a binary name with an  underscore
is passed to config the part after the underscore is considered a subcommand.
The environment variables for subcommands start with [BINARY]_[SUBCOMMAND]_CONFIG_
when a subcommand is called the options/configuration for the binary are also loaded.


*/

/*
func main() {
	flag.Parse()
	fmt.Printf("%#v\n", os.Args)
	fmt.Printf("%#v\n", flag.Args())
}
*/
