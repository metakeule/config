package config

import (
	"fmt"
	"io/ioutil"
	"path/filepath"
	"sort"
	"strings"

	"gitlab.com/metakeule/version"
)

const (
	MAJOR = "maj"
	MINOR = "min"
	PATCH = "patch"
)

func getGittag(wd string) (v *version.Version, err error) {

	/*
		var c exec.CMD
		c.Wdx = "test"
		return "", nil
	*/
	cmd := execGetGitTags()
	cmd.Dir = wd
	out, err := cmd.CombinedOutput()
	if err != nil {
		return nil, err
	}

	v, err2 := findLastVersionFromGittags(strings.TrimSpace(string(out)))
	if err2 != nil {
		return nil, err2
	}

	return v, nil
}

func Versionate(file string, v *version.Version) (file_versionated string) {
	return fmt.Sprintf("%s_%v_%v_%v", file, v.Major, v.Minor, v.Patch)
}

func _versionate(wd, file string) (file_versionated string, err error) {
	v, err := readVERSIONFile(wd)
	if err != nil {
		return "", fmt.Errorf("can't read VERSION file: %s", err.Error())
	}

	return Versionate(file, v), nil
}

/*
type Version struct {
	Major int
	Minor int
	Patch int
}

func (v Version) String() string {
	return fmt.Sprintf("%v.%v.%v", v.Major, v.Minor, v.Patch)
}

func ParseVersion(v string) (*Version, error) {
	v = strings.TrimLeft(v, "v")
	nums := strings.Split(v, ".")
	var ns = make([]int, len(nums))

	var ver Version

	for i, n := range nums {
		nn, err := strconv.Atoi(n)
		if err != nil {
			return nil, err
		}
		ns[i] = nn
	}

	switch len(ns) {
	case 1:
		ver.Major = ns[0]
	case 2:
		ver.Major = ns[0]
		ver.Minor = ns[1]
	case 3:
		ver.Major = ns[0]
		ver.Minor = ns[1]
		ver.Patch = ns[2]
	default:
		return nil, fmt.Errorf("invalid version string %q", v)
	}

	return &ver, nil

}
*/

/*
type versions []*Version

func (v versions) Less(a, b int) bool {
	if v[a].Major != v[b].Major {
		return v[a].Major < v[b].Major
	}

	if v[a].Minor != v[b].Minor {
		return v[a].Minor < v[b].Minor
	}

	return v[a].Patch < v[b].Patch
}

func (v versions) Len() int {
	return len(v)
}

func (v versions) Swap(a, b int) {
	v[a], v[b] = v[b], v[a]
}

func (v versions) Last() *Version {
	if len(v) == 0 {
		return nil
	}

	return v[len(v)-1]
}
*/

const VERSION_FILE = "VERSION"
const VERSION_GO_FILE = "version.go"

func readVERSIONFile(wd string) (v *version.Version, err error) {
	res, err1 := ioutil.ReadFile(filepath.Join(wd, VERSION_FILE))
	if err1 != nil {
		return nil, err1
	}

	v, err = version.Parse(strings.TrimSpace(string(res)))
	//fmt.Printf("parsing from VERSION file: %v\n", v)
	if err != nil {
		v = nil
	}

	return
}

func writeVERSIONFile(wd string, v *version.Version) error {
	return ioutil.WriteFile(filepath.Join(wd, VERSION_FILE), []byte(v.String()+"\n"), 0644)
}

func writeGoFile(wd string, v *version.Version, pkg string) error {
	str := "package " + pkg + "\nconst VERSION=\"" + v.String() + "\"\n"
	return ioutil.WriteFile(filepath.Join(wd, VERSION_GO_FILE), []byte(str), 0644)
}

func findLastVersionFromGittags(tags string) (*version.Version, error) {
	//return nil, nil
	var vs version.Versions
	lines := strings.Split(tags, "\n")

	for _, line := range lines {
		ver, err := version.Parse(line)
		if err == nil {
			vs = append(vs, ver)
		}
	}

	if len(vs) == 0 {
		return nil, fmt.Errorf("no versions found")
	}

	sort.Sort(vs)

	return vs.Last(), nil
}

func setGittag(wd string, v *version.Version) (err error) {
	return runCMDs(wd,
		execGitAddVersionFiles(),
		execGitCommit(v.String()),
		execSetGitTag(v.String()),
	)
}
