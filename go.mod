module gitlab.com/metakeule/config

go 1.16

require (
	github.com/emersion/go-appdir v1.1.2
	github.com/pelletier/go-toml v1.8.1
	gitlab.com/metakeule/fmtdate v1.2.0
	gitlab.com/metakeule/version v1.0.0 // indirect
)
