package config

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"strings"

	"gitlab.com/metakeule/version"
)

/*
env GOOS=windows GOARCH=386 CGO_ENABLED=1 CXX=i686-w64-mingw32-g++ CC=i686-w64-mingw32-gcc go build .
*/

/*
BIN_FILE_NAME_PREFIX=muskel
PROJECT_DIR=$2
#PLATFORMS=$(go tool dist list)
PLATFORMS="dragonfly/amd64 freebsd/amd64 netbsd/amd64 openbsd/amd64 darwin/amd64 linux/amd64 linux/arm linux/arm64 windows/386 windows/amd64"
*/

func goBuild(wd, goos, goarch, file string) error {
	c := execGoBuild(goos, goarch, file)
	c.Dir = wd
	o, err := c.CombinedOutput()
	if err != nil {
		return fmt.Errorf("Errors: " + string(o))
	}
	return nil
}

const PseudoPlattformWinCGO = "windows/cgo"

func goGetAllPlattforms(wd string) ([]string, error) {
	c := execGoPlattforms()
	c.Dir = wd
	o, err := c.CombinedOutput()
	if err != nil {
		return nil, err
	}
	s := strings.TrimSpace(string(o))
	return strings.Split(s, "\n"), nil
}

func goBuildAll(wd, dir, name string, plattforms []string) (errs []error) {
	for _, p := range plattforms {
		s := strings.SplitN(p, "/", 2)
		if len(s) != 2 {
			errs = append(errs, fmt.Errorf("invalid plattform: %q", p))
			continue
		}
		goos, goarch := s[0], s[1]
		d := filepath.Join(wd, dir, goos+"-"+strings.ReplaceAll(goarch, "/", "-"))
		err := os.MkdirAll(d, 0755)
		if err != nil {
			errs = append(errs, fmt.Errorf("could not make dir: %q", d))
			continue
		}
		err = goBuild(wd, goos, goarch, filepath.Join(d, name))
		if err != nil {
			errs = append(errs, fmt.Errorf("error while building for plattfrom: %q:\n%s", p, err.Error()))
		}
	}

	return
}

// plattforms must be a string like
//    linux/amd64 linux/arm windows/386 windows/amd64
// empty string builds for all plattforms
// dir is the subdirectory of wd and file is the name of the exec file
func GoBuild(wd, dir, file, plattforms string, versionate bool, versionDir string) (errs []error) {
	//fmt.Println("running go build")
	var pforms []string
	var err error
	if len(plattforms) == 0 {
		//fmt.Println("no plattforms, getting go plattforms")
		pforms, err = goGetAllPlattforms(wd)
		if err != nil {
			errs = append(errs, fmt.Errorf("could not get go plattforms: %s", err.Error()))
			return
		}
	} else {
		pforms = strings.Split(strings.TrimSpace(plattforms), " ")
	}

	if versionate {
		file, err = _versionate(versionDir, file)
		if err != nil {
			errs = append(errs, err)
			return
		}
	}

	//fmt.Printf("building for plattforms: %v\n", pforms)

	return goBuildAll(wd, dir, file, pforms)
}

func Release(wd string, typ string, gittag bool, pkg string) (oldversion, newversion string, err error) {
	/*
		TODO
		 1. get the current version somehow, sources are: VERSION file in current dir or latest git tags use whatever is higher
		 2. up the version by release type
		 3. write the version into VERSION file and version.go file
		 4. if gittag option is given, create a new gittag for the version
	*/

	vfile, _ := readVERSIONFile(wd)
	vgit, _ := getGittag(wd)

	var currentVersion *version.Version

	switch {
	case vfile == nil && vgit == nil:
		err = fmt.Errorf("could not find current version via git tag nor via VERSION file")
		return
	case vfile != nil && vgit == nil:
		currentVersion = vfile
	case vfile == nil && vgit != nil:
		currentVersion = vgit
	default:
		var vs = version.Versions{vfile, vgit}
		sort.Sort(vs)
		currentVersion = vs.Last()
	}

	oldversion = currentVersion.String()

	switch typ {
	case MAJOR:
		currentVersion.Major += 1
		currentVersion.Minor = 0
		currentVersion.Patch = 0
	case MINOR:
		currentVersion.Minor += 1
		currentVersion.Patch = 0
	case PATCH:
		currentVersion.Patch += 1
	default:
		err = fmt.Errorf("invalid release type %q", typ)
		return
	}

	newversion = currentVersion.String()

	//fmt.Printf("new version: %v\n", currentVersion)

	err = writeGoFile(wd, currentVersion, pkg)
	if err != nil {
		err = fmt.Errorf("could not write %q file: %s", VERSION_GO_FILE, err.Error())
		return
	}

	err = writeVERSIONFile(wd, currentVersion)
	if err != nil {
		err = fmt.Errorf("could not write %q file: %s", VERSION_FILE, err.Error())
		return
	}

	if gittag {
		err = setGittag(wd, currentVersion)
		if err != nil {
			err = fmt.Errorf("could not set git tag")
			return
		}
	}

	return
}
