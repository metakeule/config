// +build windows

package config

import (
	"os"
	"os/exec"
	"strings"
)

func execCommand(c string) *exec.Cmd {
	return exec.Command("cmd.exe", "/C", c)
}

func execGetGitTags() *exec.Cmd {
	return execCommand("git.exe tag")
}

func execGitAddVersionFiles() *exec.Cmd {
	return execCommand("git.exe add VERSION version.go")
}

func execGitCommit(version string) *exec.Cmd {
	return execCommand("git.exe commit -m 'release v" + version + "'")
}

func execSetGitTag(version string) *exec.Cmd {
	//return exec.Command("git.exe", "tag", "v"+version)
	return execCommand("git.exe tag v" + version)
}

func execGoBuild(goos, goarch, file string) *exec.Cmd {
	if goos == "windows" {
		file = file + ".exe"
	}
	cmd := execCommand("go.exe build -o " + file)
	//	fmt.Println("using command " + "go build -o " + file)

	sl := strings.Split(goarch, "/")
	var cgo bool
	if len(sl) == 2 && sl[1] == "cgo" {
		cgo = true
	}
	goarch = sl[0]

	//	fmt.Println("using command " + "go build -o " + file)
	env := os.Environ()
	env = append(env, "GOOS="+goos, "GOARCH="+goarch)
	if cgo {
		env = append(env, "CGO_ENABLED=1")
	}

	cmd.Env = env
	return cmd
}

func execGoPlattforms() *exec.Cmd {
	return execCommand("go.exe tool dist list")
}
