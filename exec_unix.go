// +build !windows

package config

import (
	"os"
	"os/exec"
	"strings"
)

func execCommand(c string) *exec.Cmd {
	return exec.Command("/bin/sh", "-c", c)
}

func execGetGitTags() *exec.Cmd {
	return execCommand("git tag")
}

func execGitAddVersionFiles() *exec.Cmd {
	return execCommand("git add VERSION version.go")
}

func execGitCommit(version string) *exec.Cmd {
	return execCommand("git commit -m 'release v" + version + "'")
}

func execSetGitTag(version string) *exec.Cmd {
	//return exec.Command("git", "tag", "v"+version)
	return execCommand("git tag v" + version)
}

func execGoBuild(goos, goarch, file string) *exec.Cmd {
	if goos == "windows" {
		file = file + ".exe"
	}
	cmd := execCommand("go build -o " + file)

	sl := strings.Split(goarch, "/")
	var cgo bool
	if len(sl) == 2 && sl[1] == "cgo" {
		cgo = true
	}
	goarch = sl[0]

	//	fmt.Println("using command " + "go build -o " + file)
	env := os.Environ()
	env = append(env, "GOOS="+goos, "GOARCH="+goarch)
	if cgo {
		env = append(env, "CGO_ENABLED=1")
	}

	if goos == "windows" && goarch == "386" {
		// env GOOS=windows GOARCH=386 CGO_ENABLED=1 CXX=i686-w64-mingw32-g++ CC=i686-w64-mingw32-gcc go build .
		env = append(env, "CXX=i686-w64-mingw32-g++", "CC=i686-w64-mingw32-gcc")
	}
	cmd.Env = env
	return cmd
}

func execGoPlattforms() *exec.Cmd {
	return execCommand("go tool dist list")
}
