package config

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"strconv"
	"strings"
	"time"
	//"gitlab.com/metakeule/fmtdate"
)

func convertOpttype(optType string) string {

	switch optType {
	case "bool":
		return ""
	case "int32":
		return "<integer>"
	case "float32":
		return "<float>"
	case "string":
		return "''"
	case "json":
		return "<json>"
	case "time":
		return "<hh:mm:ss>"
	case "datetime":
		return "<YYYY-MM-DD hh:mm:ss>"
	case "date":
		return "<YYYY-MM-DD>"
	}
	panic("should not happend")
	/*
		"bool"
		"int32"
		"float32"
		"string"
		"datetime"
		"date"
		"time"
		"json"
	*/
}

var leftWidth = 32
var totalWidth = 80

func pad(left string, right string) string {
	var bf bytes.Buffer

	numSpaces := leftWidth - len([]rune(left))
	bf.WriteString(left)

	for i := 0; i < numSpaces; i++ {
		bf.WriteString(" ")
	}

	spaceRight := totalWidth - 1 - leftWidth
	right = strings.Replace(right, "\n", " \n ", -1)
	// fmt.Printf("%#v\n", right)
	arr := strings.Split(right, " ")

	spaceRightAvailable := spaceRight
	for _, word := range arr {
		w := strings.TrimSpace(word)
		if w == "" && word != "\n" {
			continue
		}
		// fmt.Printf("%#v\n", word)
		// fmt.Printf(" w[0]: %#v \\n: %#v\n", w[0], '\n')
		if len(w) > spaceRightAvailable || word == "\n" {
			bf.WriteString("\n")
			for i := 0; i < leftWidth; i++ {
				bf.WriteString(" ")
			}
			spaceRightAvailable = spaceRight
		}

		bf.WriteString(w + " ")
		spaceRightAvailable -= len(w) + 1
	}

	/*
		for i, r := range right {
			if i != 0 && i%spaceRight == 0 {
				bf.WriteString("\n                           ")
			}
			bf.WriteRune(r)
			// fmt.Printf("spaceRight: %v, i+1= %v\n", spaceRight, i+1)

		}
	*/
	return bf.String()
}

func runCMDs(wd string, cmds ...*exec.Cmd) error {
	for _, c := range cmds {
		c.Dir = wd
		_, err := c.CombinedOutput()
		if err != nil {
			return err
		}
	}

	return nil
}

const (
	DateFormat     = "2006-01-02"
	TimeFormat     = "15:04:05"
	DateTimeFormat = "2006-01-02 15:04:05"
	//DateTimeGeneral = "YYYY-MM-DD hh:mm:ss"
	DateTimeGeneral = "2006-01-02 15:04:05 -0700 MST"
	//TimeGeneral    = "2006-01-02 15:04:05 +0000 UTC"
)

var (
	NameRegExp      = regexp.MustCompile("^[a-z][a-z0-9]+$")
	VersionRegexp   = regexp.MustCompile("^[a-z0-9-.]+$")
	ShortflagRegexp = regexp.MustCompile("^[a-z]$")
)

func ValidateShortflag(shortflag string) error {
	if shortflag == "" || ShortflagRegexp.MatchString(shortflag) {
		return nil
	}
	return ErrInvalidShortflag
}

// ValidateName checks if the given name conforms to the
// naming convention. If it does, nil is returned, otherwise
// ErrInvalidName is returned
func ValidateName(name string) error {
	if name == "" {
		return InvalidNameError(name)
	}

	if !NameRegExp.MatchString(name) {
		return InvalidNameError(name)
	}

	return nil
}

func ValidateVersion(version string) error {
	if !VersionRegexp.MatchString(version) {
		return InvalidVersionError(version)
	}
	return nil
}

// ValidateType checks if the given type is valid.
// If it does, nil is returned, otherwise
// ErrInvalidType is returned
func ValidateType(option Option) error {
	switch option.Type {
	case "bool", "int32", "float32", "string", "datetime", "date", "time", "json":
		return nil
	default:
		return InvalidTypeError{option}
	}
}

//var delim = []byte("\u220e\n")
var delim = []byte("\n$")

// var delim = []byte("\n\n")

func stringToValue(typ string, in string) (out interface{}, err error) {
	switch typ {
	case "bool":
		return strconv.ParseBool(in)
	case "int32":
		i, e := strconv.ParseInt(in, 10, 32)
		return int32(i), e
	case "float32":
		fl, e := strconv.ParseFloat(in, 32)
		return float32(fl), e
	case "datetime":
		//return time.Parse(DateTimeFormat, in)
		d, e := time.Parse(DateTimeGeneral, in)
		if e != nil {
			return time.Parse(DateTimeFormat, in)
		}
		return d, nil
		//return fmtdate.ParseDateTime(in)
	case "date":
		d, e := time.Parse(DateTimeGeneral, in)
		if e != nil {
			return time.Parse(DateFormat, in)
		}
		return d, nil
		//return fmtdate.ParseDateTime(in)
		//return time.Parse(DateTimeFormat, in)
	case "time":
		d, e := time.Parse(DateTimeGeneral, in)
		if e != nil {
			return time.Parse(TimeFormat, in)
		}
		return d, nil
		//return fmtdate.ParseDateTime(in)
		//return time.Parse(DateTimeFormat, in)
	case "string":
		return in, nil
	case "json":
		var v interface{}
		err = json.Unmarshal([]byte(in), &v)
		if err != nil {
			return nil, err
		}
		return in, nil
	default:
		return nil, errors.New("unknown type " + typ)
	}

}

func keyToArg(key string) string {
	return "--" + key
}

func argToKey(arg string) string {
	return strings.TrimLeft(arg, "-")
}

func err2Stderr(err error) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error: %s\n", err)
		os.Exit(1)
	}
}
