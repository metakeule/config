.PHONY: all test coverage

all: get build install

get:
	go get ./...

build:
	cd cmd/config && config build -v --versiondir='../../'

release:
	config release --versiondir='.' --package='config'
	cd cmd/config && config build -v --versiondir='../../'

test:
	go test ./... -v -coverprofile .coverage.txt
	go tool cover -func .coverage.txt

coverage: test
	go tool cover -html=.coverage.txt