package config

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"

	//"runtime/debug"
	"strings"
	"time"

	toml "github.com/pelletier/go-toml"
	"gitlab.com/metakeule/fmtdate"
)

/*
TODO make more tests
TODO improve cmdline utility
TODO improve documentation
maybe add subpackage to help getting args from the web, make this explicit
maybe add subpackage to help getting configuration from etcd
*/

type Config struct {
	helpIntro string
	app       string
	version   string
	spec      map[string]*Option
	values    map[string]interface{}
	//lastArg     interface{}
	//lastArgName string
	locations map[string][]string
	// maps shortflag to option
	shortflags    map[string]string
	commands      map[string]*Config
	activeCommand *Config

	// only for subcommands
	skippedOptions map[string]bool
	relaxedOptions map[string]bool
	parent         *Config
}

// New creates a new *Config for the given app and version
// An error is returned, if the app and the version do not not match
// the following regular expressions:
// app => NameRegExp
// version => VersionRegexp
func New(app string, version string, helpIntro string) (c *Config, err error) {

	if err = ValidateName(app); err != nil {
		err = ErrInvalidAppName(app)
		return
	}

	c = &Config{}
	c.spec = map[string]*Option{}
	c.commands = map[string]*Config{}
	c.app = app
	c.version = version
	c.shortflags = map[string]string{}
	c.helpIntro = helpIntro

	/*
	   // does not work, see https://github.com/golang/go/issues/29228
	   	info, ok := debug.ReadBuildInfo()

	   	if !ok {
	   		return nil, fmt.Errorf("can't get build info: main package does not use modules")
	   	}

	   	c.Version = info.Main.Version
	*/

	if err = ValidateVersion(c.version); err != nil {
		return nil, err
	}

	c.Reset()
	return
}

// MustNew calls New() and panics on errors
func MustNew(app string, version string, helpIntro string) *Config {
	c, err := New(app, version, helpIntro)
	if err != nil {
		panic(err)
	}
	return c
}

func (c *Config) EachSpec(fn func(name string, opt *Option)) {
	for k, opt := range c.spec {
		fn(k, opt)
	}
}

func (c *Config) EachValue(fn func(name string, val interface{})) {
	for k, val := range c.values {
		if k == "" {
			return
		}
		fn(k, val)
	}
}

func (c *Config) EachCommand(fn func(name string, val *Config)) {
	for k, val := range c.commands {
		fn(k, val)
	}
}

// MustSub calls Sub() and panics on errors
func (c *Config) MustCommand(name string, helpIntro string) *Config {
	s, err := c.Command(name, helpIntro)
	if err != nil {
		panic(err)
	}
	return s
}

func (c *Config) SpecTree() map[string]*Option {
	stree := map[string]*Option{}

	for k, v := range c.spec {
		stree[k] = v
	}

	for n, cmd := range c.commands {
		for kk, vv := range cmd.spec {
			stree[n+"."+kk] = vv
		}
	}

	return stree
}

func (c *Config) lastArgName() string {
	if last, has := c.spec[""]; has {
		return last.LastArgName
	}
	return ""
}

// Skip skips all options of the parent command but the given
func (c *Config) SkipAllBut(except ...string) *Config {
	if !c.isCommand() {
		panic("can only Skip in subcommands")
	}

	ex := map[string]bool{}
	parentLastArgName := c.parent.lastArgName()

	for _, x := range except {
		if x != "" && x == parentLastArgName {
			//if x == c.parent.lastArgName {
			ex[""] = true
		} else {
			ex[x] = true
		}
	}

	for k := range c.parent.spec {
		if ex[k] {
			continue
		}

		c.skippedOptions[k] = true
	}

	return c
}

// Skip skips the given option of the parent command and is chainable
// It panics, if the given option is not a parent option of if the
// current config is no subcommand
func (c *Config) Skip(option string) *Config {
	if !c.isCommand() {
		panic("can only Skip in subcommands")
	}

	opt := option

	if option == c.parent.lastArgName() {
		opt = ""
	}

	_, has := c.parent.spec[opt]
	if !has {
		panic("option " + option + " is not a general option")
	}
	c.skippedOptions[opt] = true
	return c
}

func (c *Config) Relax(option string) *Config {
	if !c.isCommand() {
		panic("can only Relax in subcommands")
	}

	opt := option

	if option == c.parent.lastArgName() {
		opt = ""
	}

	_, has := c.parent.spec[opt]
	if !has {
		panic("option " + option + " is not a general option")
	}
	c.relaxedOptions[opt] = true
	return c
}

func (c *Config) GetCommandConfig(name string) (s *Config, err error) {
	var has bool
	s, has = c.commands[name]
	if !has {
		return nil, fmt.Errorf("no such command %q", name)
	}
	return s, nil
}

// Sub returns a *Config for a subcommand.
// If name does not match to NameRegExp, an error is returned
func (c *Config) Command(name string, helpIntro string) (s *Config, err error) {
	if c.isCommand() {
		err = ErrCommandCommand
		return
	}
	s, err = New(name, c.version, helpIntro)
	if err != nil {
		return
	}
	s.skippedOptions = map[string]bool{}
	s.relaxedOptions = map[string]bool{}

	s.app = c.app + "_" + s.app
	s.parent = c
	c.commands[name] = s

	return s, nil
}

// addOption adds the given option, validates it and returns any error
func (c *Config) addOption(opt *Option) error {
	if opt.Name != "" {
		if err := ValidateName(opt.Name); err != nil {
			return ErrInvalidOptionName(opt.Name)
		}
	}

	if _, has := c.spec[opt.Name]; has {
		return ErrDoubleOption(opt.flagName())
	}
	c.spec[opt.Name] = opt
	if opt.Shortflag != "" {
		if _, has := c.shortflags[opt.Shortflag]; has {
			return ErrDoubleShortflag(opt.Shortflag)
		}
		c.shortflags[opt.Shortflag] = opt.Name
	}
	return nil
}

// Reset cleans the values, the locations and any current subcommand
func (c *Config) Reset() {
	c.values = map[string]interface{}{}
	//c.lastArg = nil
	c.locations = map[string][]string{}
	c.activeCommand = nil
}

// Location returns the locations where the option was set in the order of setting.
//
// The locations are tracked differently:
// - defaults are tracked by their %v printed value
// - environment variables are tracked by their name
// - config files are tracked by their path
// - cli args are tracked by their name
// - settings via Set() are tracked by the given location or the caller if that is empty
func (c *Config) Locations(option string) []string {
	if option == c.lastArgName() || option == "" {
		return nil
	}
	if err := ValidateName(option); err != nil {
		panic(InvalidNameError(option))
	}
	return c.locations[option]
}

// IsOption returns true, if the given option is allowed
func (c *Config) IsOption(option string) bool {
	if option == c.lastArgName() {
		option = ""
	}

	if option != "" {
		if err := ValidateName(option); err != nil {
			return false
		}
	}
	_, has := c.spec[option]
	return has
}

func (c *Config) setLastArg(arg string) error {
	spec, has := c.spec[""]

	if !has {
		if c.isCommand() {
			if c.skippedOptions[""] {
				return UnknownOptionError{c.version, ""}
			}
			if _, has := c.parent.spec[""]; has {
				//fmt.Printf("skipping %q\n", arg)
				return nil
			} else {
				return UnknownOptionError{c.version, ""}
			}
		} else {
			return UnknownOptionError{c.version, ""}
		}
	}

	if c.values[""] != nil {
		return fmt.Errorf("last argument already set to %#v", c.values[""])
	}

	out, err := stringToValue(spec.Type, arg)

	if err != nil {
		return InvalidValueError{*spec, arg}
	}

	c.values[""] = out

	return nil
}

// set sets the option to the value and validates the value returning any errors
func (c *Config) set(option string, value string, location string) error {
	if err := ValidateName(option); err != nil {
		return InvalidNameError(option)
	}
	spec, has := c.spec[option]

	if !has {
		return UnknownOptionError{c.version, option}
	}

	out, err := stringToValue(spec.Type, value)

	if err != nil {
		return InvalidValueError{*spec, value}
	}

	c.values[option] = out
	c.locations[option] = append(c.locations[option], location)
	return nil
}

// Set sets the option to the value. Location is a hint from where the
// option setting was triggered. If the location is empty, the caller file
// and line is tracked as location.
func (c *Config) Set(option string, val string, location string) error {
	if location == "" {
		_, file, line, _ := runtime.Caller(0)
		location = fmt.Sprintf("%s:%d", file, line)
	}
	return c.set(option, val, location)
}

// setMap sets the given options and tracks the calling function as
// location
func (c *Config) setMap(options map[string]string) error {
	_, file, line, _ := runtime.Caller(1)
	location := fmt.Sprintf("%s:%d", file, line)

	for opt, val := range options {
		err := c.set(opt, val, location)
		if err != nil {
			return err
		}
	}
	return nil
}

// IsSet returns true, if the given option is set and false if not.
func (c Config) IsSet(option string) bool {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	_, has := c.values[option]
	return has
}

// CheckMissing checks if mandatory values are missing inside the values map
// CheckMissing stops on the first error
func (c *Config) CheckMissing() error {
	empty := map[string]bool{}
	return c.checkMissing(empty, empty)
}

// CheckMissing checks if mandatory values are missing inside the values map
// CheckMissing stops on the first error
func (c *Config) checkMissing(skippedOptions map[string]bool, relaxedOptions map[string]bool) error {
	for k, spec := range c.spec {
		if spec != nil && spec.Required && spec.Default == nil {
			if _, has := skippedOptions[k]; has {
				continue
			}
			if _, has := relaxedOptions[k]; has {
				continue
			}

			if _, has := c.values[k]; !has {
				return MissingOptionError{c.version, *spec}
			}
		}
	}
	return nil
}

// ValidateValues validates only values that are set and not nil.
// It does not check for missing mandatory values (use CheckMissing for that)
// ValidateValues stops on the first error
func (c *Config) ValidateValues() error {
	for k, v := range c.values {
		if v == nil {
			continue
		}
		spec, has := c.spec[k]
		if !has {
			return UnknownOptionError{c.version, k}
			// return errors.New("unkown config key " + k)
		}
		if err := spec.ValidateValue(v); err != nil {
			return InvalidConfig{c.version, err}
		}
	}
	return nil
}

// CurrentSub returns the active command
func (c *Config) ActiveCommand() (s *Config) {
	return c.activeCommand
}

// isCommand checks if the *Config relongs to a subcommand
func (c *Config) isCommand() bool {
	return !(strings.Index(c.app, "_") == -1)
}

// MarshalJSON serializes the spec to JSON
func (c *Config) MarshalJSON() ([]byte, error) {
	stree := c.SpecTree()
	return json.Marshal(stree)
}

// UnmarshalJSON deserializes the spec from JSON
func (c *Config) UnmarshalJSON(data []byte) error {
	stree := map[string]*Option{}

	err := json.Unmarshal(data, &stree)

	if err != nil {
		return err
	}

	c.spec = map[string]*Option{}

	for k, v := range stree {
		ks := strings.Split(k, ".")
		switch len(ks) {
		case 1:
			c.spec[k] = v
		case 2:
			sub, has := c.commands[ks[0]]
			if !has {
				sub, err = c.Command(ks[0], "")
				/*
					sub = &Config{
						spec: map[string]*Option{},
					}
				*/
				//c.commands[ks[0]] = sub
				if err != nil {
					return fmt.Errorf("can't unmarshall command %q: %v", ks[0], err)
				}
			}
			sub.spec[ks[1]] = v
		default:
			return fmt.Errorf("invalid key for unmarshalling (contains more than one dot: %q", k)
		}

	}

	return nil
}

// appName returns the name of the app
func (c *Config) appName() string {
	if c.isCommand() {
		return c.app[:strings.Index(c.app, "_")]
	}
	return c.app
}

func (c *Config) CommmandName() string {
	return c.commandName()
}

// commandName returns the name of the subcommand and the empty string, if there is no subcommand, the empty string is returned
func (c *Config) commandName() string {
	if c.isCommand() {
		return c.app[strings.Index(c.app, "_")+1:]
	}
	return ""
}

// Binary returns the path to the binary of the app
func (c *Config) Binary() (path string, err error) {
	return exec.LookPath(c.appName())
}

func (c *Config) Merge(rd io.Reader, location string) error {
	wrapErr := func(err error) error {
		return InvalidConfigFileError{location, c.version, err}
	}

	t, err := toml.LoadReader(rd)

	if err != nil {
		return wrapErr(err)
	}

	version := fmt.Sprintf("%v", t.Get("version"))

	differentVersions := version != c.version

	//var keys = map[string]bool{}
	/*
		var valBuf bytes.Buffer
		var key string
		var subcommand string
	*/

	setValue := func(subcommand string, key string, val string) error {
		if subcommand == "" {
			//fmt.Printf("setting %#v to %#v\n", key, val)
			return c.set(key, val, location)
		} else {
			//fmt.Printf("setting %#v to %#v for subcommand %#v\n", key, val, subcommand)
			sub, has := c.commands[subcommand]
			if !has {
				return errors.New("unknown subcommand " + subcommand)
			} else {
				err = sub.set(key, val, location)
			}

			if err != nil {

				if differentVersions {
					return wrapErr(fmt.Errorf("value %#v of option %s, present in config for version %s is not valid for running version %s",
						val, key, version, c.version))
				} else {
					return wrapErr(err)
				}
			}
		}
		return nil
	}

	var getValString = func(ttt *toml.Tree, pth string) string {
		v := ttt.Get(pth)

		//fmt.Printf("%q -> %v [%T]\n", k, v, v)

		switch x := v.(type) {
		case time.Time:
			//return fmtdate.Parse(DateTimeGeneral, in)
			//val = fmtdate.Format(DateTimeGeneral, x)
			//val = time.Format(DateTimeGeneral, x)
			//val = fmtdate.FormatDateTime(*x)
			return x.Format(DateTimeGeneral)
		default:
			return fmt.Sprintf("%v", v)
		}

	}

	for _, k := range t.Keys() {
		if k == "version" {
			continue
		}

		vvvv := t.Get(k)
		//fmt.Printf("pth: %#v: %T\n", k, vvvv)

		if tt, isTree := vvvv.(*toml.Tree); isTree {
			for _, kk := range tt.Keys() {
				val := getValString(tt, kk)
				err = setValue(k, kk, val)
				if err != nil {
					return wrapErr(fmt.Errorf("can't set %q to %q for subcommand %q: %v", kk, val, k, err))
				}
			}
		} else {
			val := getValString(t, k)
			err = setValue("", k, val)
			if err != nil {
				return wrapErr(fmt.Errorf("can't set %q to %q: %v", k, val, err))
			}
		}
	}

	return nil
}

func (c *Config) MergeEnv() error {
	prefix := strings.ToUpper(c.app) + "_CONFIG_"
	// fmt.Printf("looking for prefix %#v\n", prefix)
	for _, pair := range ENV {
		if strings.HasPrefix(pair, prefix) {
			//fmt.Printf("Env: %#v\n", pair)
			startKey := len(prefix) // strings.Index(pair, prefix)
			if startKey > 0 {
				startVal := strings.Index(pair, "=")
				key, val := pair[startKey:startVal], pair[startVal+1:]
				val = strings.TrimSpace(val)

				if val == "" {
					return EmptyValueError(key)
				}

				keys := strings.Split(key, "_")

				//fmt.Printf("keys: %v\n", keys)

				switch len(keys) {
				case 1:
					//fmt.Printf("key %#v val %#v\n", key, val)
					err := c.set(strings.ToLower(keys[0]), val, pair[:startVal])
					if err != nil {
						return InvalidConfigEnv{c.version, pair[:startVal], err}
					}
				case 2:
					cconf, err := c.GetCommandConfig(strings.ToLower(keys[0]))
					if err != nil {
						return InvalidConfigEnv{c.version, pair[:startVal], fmt.Errorf("command not found: %q", strings.ToLower(keys[0]))}
					}

					err = cconf.set(strings.ToLower(keys[1]), val, pair[:startVal])
					if err != nil {
						return InvalidConfigEnv{c.version, pair[:startVal], err}
					}
				default:
					return InvalidConfigEnv{c.version, pair[:startVal], fmt.Errorf("invalid key: %q", key)}

				}
			}

		}
	}
	return nil
}

// MergeArgs merges the os.Args into the config
// args like --a-key='a val' will correspond to the config value
// A_KEY=a val
// If the key is CONFIG_SPEC, MergeArgs will print the config spec as json
// and exit the program
// If any error happens the error will be printed to os.StdErr and the program exists will
// status code 1
// exiting the program. also if --config_spec is set the spec is directly written to the
// StdOut and the program is exiting. If --help is set, the help message is printed with the
// the help  messages for the config options. If --version is set, the version of the running app is returned
func (c *Config) MergeArgs() error {
	empty := map[string]bool{}
	skipped := empty
	relaxed := empty
	if c.isCommand() {
		skipped = c.skippedOptions
		relaxed = c.relaxedOptions
	}
	_, err := c.mergeArgs(false, ARGS, skipped, relaxed)
	return err
}

func (c *Config) usageOptions(addGeneral bool, skipped map[string]bool, relaxed map[string]bool) string {
	var optBf bytes.Buffer

	for optName, opt := range c.spec {
		if _, has := skipped[optName]; has {
			continue
		}
		optBf.WriteString("\n")

		var left bytes.Buffer
		if _, has := relaxed[optName]; has || !opt.Required {
			left.WriteString("[")
		}

		if opt.Shortflag != "" {
			left.WriteString("-" + opt.Shortflag + ", ")
		}

		left.WriteString(opt.flagName())
		/*
			if optName == "" {
				if _, has := relaxed[optName]; !has && opt.Required {
					left.WriteString("<" + c.lastArgName() + ">")
				} else {
					left.WriteString(c.lastArgName())
				}
			} else {
				left.WriteString("--" + optName)
			}
		*/

		if opt.Default != nil {

			switch opt.Type {
			case "string":
				left.WriteString(fmt.Sprintf("='%s'", opt.Default))
			case "bool":
				if opt.Default.(bool) {
					left.WriteString("=true")
				} else {
					left.WriteString("=false")
				}
			case "json":
				left.WriteString(fmt.Sprintf("='%s'", opt.Default))
			case "time":
				left.WriteString(fmt.Sprintf("='%s'", fmtdate.Format("hh:mm:ss", opt.Default.(time.Time))))
			case "date":
				left.WriteString(fmt.Sprintf("='%s'", fmtdate.Format("YYYY-MM-DD", opt.Default.(time.Time))))
			case "datetime":
				left.WriteString(fmt.Sprintf("='%s'", fmtdate.Format("YYYY-MM-DD hh:mm:ss", opt.Default.(time.Time))))
			default:
				left.WriteString(fmt.Sprintf("=%v", opt.Default))

			}

		} else {
			if optName != "" {
				if opt.Type != "bool" {
					left.WriteString(fmt.Sprintf("=%s", convertOpttype(opt.Type)))
				}
			}
		}

		/*
			if opt.Required {
				left.WriteString(" (required)")
			}
		*/
		if _, has := relaxed[optName]; has || !opt.Required {
			left.WriteString("]")
		}

		optBf.WriteString(pad("  "+left.String(), opt.Help))
		optBf.WriteString("\n")
		//optBf.WriteString("\t\t" + strings.Join(strings.Split(opt.Help, "\n"), "\n\t\t"))
	}

	if !c.isCommand() && addGeneral {
		generalOptions := map[string]string{
			"version":          "prints the current version of the program",
			"help":             "prints the help",
			"config-spec":      "prints the specification of the configurable options",
			"config-env":       "prints the environmental variables of the configurable options",
			"config-locations": "prints the locations of current configuration",
			"config-files":     "prints the locations of the config files",
		}

		for optname, opthelp := range generalOptions {
			optBf.WriteString("\n" + pad("  [--"+optname+"]", opthelp))
			optBf.WriteString("\n")
		}
	}

	return optBf.String()
}

func (c *Config) lastArgHelpString(required bool) (lastArg string) {
	if sp, has := c.spec[""]; has {
		return sp.flagName()
		/*
			lastArg = c.lastArgName

			if lastArg == "" {
				lastArg = sp.Type
			}

			if required {
				lastArg = "<" + lastArg + ">"
			} else {
				lastArg = "[" + lastArg + "]"
			}
		*/
	}
	return ""
}

func (c *Config) Usage() string {
	/*
			usage: git [--version] [--help] [-C <path>] [-c name=value]
		           [--exec-path[=<path>]] [--html-path] [--man-path] [--info-path]
		           [-p|--paginate|--no-pager] [--no-replace-objects] [--bare]
		           [--git-dir=<path>] [--work-tree=<path>] [--namespace=<name>]
		           <command> [<args>]
	*/

	var commands string
	var options string
	var helpintro = "\n" + c.appName() + " v" + c.version + "\n  "
	var lastArg string

	if sp, has := c.spec[""]; has {
		lastArg = c.lastArgHelpString(sp.Required)
	}

	if !c.isCommand() {
		options = c.usageOptions(true, map[string]bool{}, map[string]bool{})
		helpintro = helpintro + c.helpIntro
	} else {
		parentOpts := c.parent.usageOptions(false, c.skippedOptions, c.relaxedOptions)

		options = c.usageOptions(false, map[string]bool{}, map[string]bool{}) + parentOpts
		helpintro = helpintro + c.parent.helpIntro

		if lastArg == "" && !c.skippedOptions[""] {
			var required bool
			if sp, has := c.parent.spec[""]; has {
				required = sp.Required
				if c.relaxedOptions[""] {
					required = false
				}
				lastArg = c.parent.lastArgHelpString(required)
			}
		}
	}
	// var subcmdIntro string

	// if len(c.subcommands) > 0 {

	// subcmdIntro = fmt.Sprintf("\nor     %s <command> OPTION...", c.appName())

	if c.isCommand() {
		if options == "" {
			return fmt.Sprintf(`%s

%s %s
  %s

usage: 
  %s %s %s

`, helpintro, c.appName(), c.commandName(), c.helpIntro, c.appName(), c.commandName(), lastArg)
		}
		return fmt.Sprintf(`%s

%s %s
  %s

usage: 
  %s %s OPTION... %s

options:%s`, helpintro, c.appName(), c.commandName(), c.helpIntro, c.appName(), c.commandName(), lastArg, options)
	}

	var cmdStr string
	var generalStr string
	var subcBf bytes.Buffer
	for subCname, subC := range c.commands {
		// subcBf.WriteString("\n  " + subCname + "\t\t" + strings.Join(strings.Split(subC.helpIntro, "\n"), "\n\t\t\t"))
		subcBf.WriteString(pad("  "+subCname, subC.helpIntro) + "\n")
		subcBf.WriteString("\n")
	}

	// }
	/*
		generalcommand := map[string]string{
			"config-spec":      "prints the specification of the configurable options",
			"config-locations": "prints the locations of current configuration",
			"config-files":     "prints the locations of the config files",
		}

		for subCname, subHelp := range generalcommand {
			subcBf.WriteString(pad("  "+subCname, subHelp) + "\n")
		}
	*/
	if len(c.commands) > 0 {

		commands = "commands:\n" + subcBf.String() + "\nfor help about a specific command, run " +
			fmt.Sprintf("\n  %s help <command>", c.appName())
		cmdStr = " [command]"
		//generalStr = "general "
		generalStr = ""
	}

	return fmt.Sprintf(`%s

usage: 
  %s%s OPTION... %s

%soptions:%s

%s
           	`, helpintro, c.appName(), cmdStr, lastArg, generalStr, options, commands)
}

func (c *Config) env_var(optName string) string {
	return strings.ToUpper(c.app + "_CONFIG_" + optName)
}

func (c *Config) envVars() []string {
	v := []string{}
	for k := range c.spec {
		if k == "" {
			continue
		}
		v = append(v, c.env_var(k))
	}
	return v
}

func (c *Config) mergeArgs(ignoreUnknown bool, args []string, skippedOptions map[string]bool, relaxedOptions map[string]bool) (merged map[string]bool, err error) {
	merged = map[string]bool{}
	// prevent duplicates
	keys := map[string]bool{}
	//fmt.Printf("args: %#v\n", args)
	//fmt.Printf("os-args: %#v\n", os.Args[1:])
	var lastArgsIdx int = -1

	for i, pair := range args {
		wrapErr := func(err error) error {
			return InvalidConfigFlag{c.version, pair, err}
		}
		idx := strings.Index(pair, "=")
		var key, val string
		if idx != -1 {
			if !(idx < len(pair)-1) {
				err = wrapErr(fmt.Errorf("invalid argument syntax at %#v\n", pair))
				return
			}
			key, val = pair[:idx], pair[idx+1:]

			if val == "" {
				err = EmptyValueError(key)
				return
			}
		} else {
			key = pair
			val = "true"
		}

		argKey := key
		key = argToKey(argKey)

		if argKey != key {
			if lastArgsIdx >= 0 {
				err = wrapErr(fmt.Errorf("invalid option %s\n", args[lastArgsIdx]))
				return
			}
		} else {
			if lastArgsIdx >= 0 {
				err = wrapErr(fmt.Errorf("last argument already set to %q\n", args[lastArgsIdx]))
				return
			}
			lastArgsIdx = i
		}

		//fmt.Println(argKey)

		switch key {

		case "config-env":
			all := c.envVars()
			for _, cmd := range c.commands {
				all = append(all, cmd.envVars()...)
			}

			for _, env := range all {
				fmt.Fprintf(os.Stdout, "%s\n", env)
			}

			os.Exit(0)

		case "config-spec":
			var bt []byte
			bt, err = c.MarshalJSON()
			if err != nil {
				err = wrapErr(fmt.Errorf("can't serialize config spec to json: %#v\n", err.Error()))
				return
			}
			fmt.Fprintf(os.Stdout, "%s\n", bt)
			os.Exit(0)

		case "config-locations":
			var bt []byte
			bt, err = json.Marshal(c.locations)
			if err != nil {
				err = wrapErr(fmt.Errorf("can't serialize config locations to json: %#v\n", err.Error()))
				return
			}
			fmt.Fprintf(os.Stdout, "%s\n", bt)
			os.Exit(0)
		case "config-files":
			cfgFiles := struct {
				Global string `json:"global,omitempty"`
				User   string `json:"user,omitempty"`
				Local  string `json:"local,omitempty"`
			}{
				c.FirstGlobalsFile(),
				c.UserFile(),
				c.LocalFile(),
			}
			var bt []byte
			bt, err = json.Marshal(cfgFiles)
			if err != nil {
				err = wrapErr(fmt.Errorf("can't serialize config files to json: %#v\n", err.Error()))
				return
			}
			fmt.Fprintf(os.Stdout, "%s\n", bt)
			os.Exit(0)
		case "version":
			fmt.Fprintf(os.Stdout, "%s version %s\n", c.appName(), c.version)
			os.Exit(0)
		case "help":
			if i+1 < len(args) {
				subc := args[i+1]
				sub, has := c.commands[subc]
				if !has {
					err = wrapErr(fmt.Errorf("unknown subcommand: %#v\n", subc))
					return
				}

				fmt.Fprintf(os.Stdout, "%s\n", sub.Usage())
				os.Exit(0)
			}
			fmt.Fprintf(os.Stdout, "%s\n", c.Usage())
			os.Exit(0)
		case argKey:
			_, has := c.spec[""]
			if ignoreUnknown && !has {
				continue
			}
			err = c.setLastArg(argKey)
			if err != nil {
				err = wrapErr(fmt.Errorf("invalid value %s: %s\n", key, err.Error()))
				return
			}
			merged[argKey] = true
		default:
			if sh, has := c.shortflags[key]; has {
				key = sh
			}

			if keys[key] {
				err = ErrDoubleOption("-" + key)
				return
			}

			// fmt.Println(key)
			_, has := c.spec[key]
			if ignoreUnknown && !has {
				continue
			}
			err = c.set(key, val, argKey)
			if err != nil {
				err = wrapErr(fmt.Errorf("invalid value for option %s: %s\n", key, err.Error()))
				return
			}
			merged[argKey] = true
			keys[key] = true
		}
	}

	if err = c.ValidateValues(); err != nil {
		return
	}
	err = c.checkMissing(skippedOptions, relaxedOptions)
	return
}

// GetBool returns the value of the option as bool
func (c Config) GetBool(option string) bool {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(bool)
	}
	return false
}

// GetFloat32 returns the value of the option as float32
func (c Config) GetFloat32(option string) float32 {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(float32)
	}
	return 0
}

// GetInt32 returns the value of the option as int32
func (c Config) GetInt32(option string) int32 {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(int32)
	}
	return 0
}

// GetValue returns the value of the option
func (c Config) GetValue(option string) interface{} {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v
	}
	return nil
}

// GetTime returns the value of the option as time
func (c Config) GetTime(option string) (t time.Time) {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		t = v.(time.Time)
	}
	return
}

// GetString returns the value of the option as string
func (c Config) GetString(option string) string {
	if option != "" {
		if err := ValidateName(option); err != nil {
			panic(InvalidNameError(option))
		}
	}
	v, has := c.values[option]
	if has {
		return v.(string)
	}
	return ""
}

// GetJSON unmarshals the value of the option to val.
func (c Config) GetJSON(option string, val interface{}) error {
	if err := ValidateName(option); err != nil {
		panic(InvalidNameError(option))
	}
	v, has := c.values[option]
	if has {
		return json.Unmarshal([]byte(v.(string)), val)
	}
	return nil
}

func (c *Config) noValues() bool {
	if len(c.values) > 0 {
		return false
	}

	for _, cc := range c.commands {
		if len(cc.values) > 0 {
			return false
		}
	}

	return true
}

// WriteConfigFile writes the configuration values to the given file
// The file is overwritten/created on success and a backup of an existing file is written back
// if an error happens
// the given perm is only used to create new files.
func (c *Config) WriteConfigFile(path string, perm os.FileMode) (err error) {
	if c.isCommand() {
		return errors.New("WriteConfigFile must not be called in sub command")
	}
	if errValid := c.ValidateValues(); errValid != nil {
		return errValid
	}
	dir := filepath.FromSlash(filepath.Dir(path))
	info, errDir := os.Stat(dir)

	if errDir == nil && !info.IsDir() {
		return fmt.Errorf("%s is no directory", dir)
	}

	if os.IsNotExist(errDir) {
		errDir = os.MkdirAll(dir, 0755)
	}

	if errDir != nil {
		return errDir
	}

	path = filepath.FromSlash(path)

	backup, errBackup := ioutil.ReadFile(path)
	backupInfo, errInfo := os.Stat(path)
	// don't write anything, if we have no config values
	if c.noValues() {
		// files exist, but will be deleted (no config values)
		if errInfo == nil {
			return os.Remove(path)
		}
		// files does not exist, we have no values, so lets do nothing
		return nil
	}
	if errBackup != nil {
		backup = []byte{}
	}
	if errInfo == nil {
		perm = backupInfo.Mode()
	}
	file, errCreate := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, perm)
	if errCreate != nil {
		return errCreate
	}

	defer func() {
		file.Close()
		if err != nil {
			os.Remove(path)
			if len(backup) != 0 {
				ioutil.WriteFile(path, backup, perm)
			}
		}
	}()

	return c.writeConfigValues(file)
}

func (c *Config) writeConfigValues(file *os.File) error {

	//te := toml.NewEncoder(file)
	defer file.Close()

	m := map[string]interface{}{}
	m["version"] = c.version

	for k, v := range c.values {
		if k == "" {
			continue
		}
		// do nothing for nil values
		if v == nil {
			continue
		}

		/*
			help := strings.Split(c.spec[k].Help, "\n")
			helplines := []string{}

			for _, h := range help {
				helplines = append(helplines, strings.TrimSpace(h))
			}
		*/

		m[k] = v
	}

	for cmd, cmdConfig := range c.commands {
		subm := map[string]interface{}{}
		for kk, vv := range cmdConfig.values {
			if kk == "" {
				continue
			}
			if vv == nil {
				continue
			}

			subm[kk] = vv

			//m[cmd+"."+kk] = vv
		}

		if len(subm) > 0 {
			m[cmd] = subm
		}

	}

	prom := toml.NewEncoder(file).PromoteAnonymous(true)
	if err := prom.Encode(m); err != nil {
		return err
	}

	return nil
}
